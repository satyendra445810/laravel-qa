<a title="Click to mark as favourite Question (Click again to undo)"
   class="favourite mt-2 {{ Auth::guest() ? 'off' : ($model->is_favourited ? 'favourite' : '') }}"
   onclick="event.preventDefault(); document.getElementById('favourite-question-{{ $model->id }}').submit();">

    <i class="fa fa-star fa-2x"></i>
    <span class="favourite-count">{{ $model->favourites_count }}</span>
</a>
<form action="/questions/{{ $model->id }}/favourites" method="post"
      id="favourite-question-{{ $model->id }}" style="display: none;">--}}
    @csrf
    @if($model->is_favourited)
        @method ('DELETE')
    @endif
</form>
